package app

import (
	"errors"
	"reflect"
)

func Multiply(a float32, b float32) float32 {
	return a * b
}

func IsInteger(a interface{}) (bool, error) {
	if reflect.TypeOf(a) == reflect.TypeOf(1) {
		return true, nil
	}

	return false, errors.New("Not an Integer!")
}

func IntToDayName(a int) string {
	days := []string{"senin", "selasa", "rabu", "kamis", "jumat", "sabtu", "minggu"}

	return days[a-1]
}

func DayNameToInt(a string) int {
	days := map[string]int{"senin": 1, "selasa": 2, "rabu": 3, "kamis": 4, "jumat": 5, "sabtu": 6, "minggu": 7}

	return days[a]
}

func IsiHello(a *string) {
	var tmp *string = a
	*tmp = "hello world"
}
